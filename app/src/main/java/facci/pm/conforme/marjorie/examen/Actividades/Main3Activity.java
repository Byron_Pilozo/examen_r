package facci.pm.conforme.marjorie.examen.Actividades;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import facci.pm.conforme.marjorie.examen.R;

public class Main3Activity extends AppCompatActivity {

    private TextView nombres, apellidos, parcial1, parcial2;
    private ImageView foto;
    private static final  String URL_E = "http://10.1.15.127:3005/api/estudiante/";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main3);
        String id = getIntent().getStringExtra("id");
        nombres = (TextView)findViewById(R.id.LBLNombresE);
        apellidos = (TextView)findViewById(R.id.LBLApellidosE);
        parcial1 = (TextView)findViewById(R.id.LBLParcial1E);
        parcial2 = (TextView)findViewById(R.id.LBLPArcial2EE);
        foto = (ImageView)findViewById(R.id.ImagenEstudiantee);
        Datos(id);
    }

    private void Datos(String id) {

        StringRequest stringRequest = new StringRequest(Request.Method.GET, URL_E+ id, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    Log.e("gfvfdds", response);
                    nombres.setText("Nombres E: " + jsonObject.getString("nombres"));
                    apellidos.setText("Apellidos E: " + jsonObject.getString("apellidos"));
                    parcial1.setText("Parcial 1: " + jsonObject.getString("parcial_uno"));
                    parcial2.setText("Parcial 2: "+String.valueOf(jsonObject.getString("parcial_dos")));
                    Picasso.get().load(jsonObject.getString("imagen")).into(foto);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }
}
