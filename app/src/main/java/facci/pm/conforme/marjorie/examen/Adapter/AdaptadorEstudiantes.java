package facci.pm.conforme.marjorie.examen.Adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import facci.pm.conforme.marjorie.examen.Actividades.Main3Activity;
import facci.pm.conforme.marjorie.examen.Modelos.Estudiantes;
import facci.pm.conforme.marjorie.examen.R;

public class AdaptadorEstudiantes extends RecyclerView.Adapter<AdaptadorEstudiantes.ViewHolder>{

    ArrayList<Estudiantes> estudiantesArrayList;

    public AdaptadorEstudiantes(ArrayList<Estudiantes> estudiantesArrayList) {
        this.estudiantesArrayList = estudiantesArrayList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.estudiante_lista, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, final int i) {

        final Estudiantes estudiantes = estudiantesArrayList.get(i);
        viewHolder.nombres.setText("NOMBRES: " + estudiantes.getNombres());
        viewHolder.apellidos.setText("APELLIDOS: " + estudiantes.getApellidos());
        viewHolder.parcial1.setText("PARCIAL UNO: " + estudiantes.getParcial_uno());
        viewHolder.parcial2.setText("PARCIAL DOS: " + estudiantes.getParcial_uno());
        Picasso.get().load(estudiantes.getImagen()).into(viewHolder.foto);
        viewHolder.view1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String id = estudiantes.getId().trim();
                Context context = v.getContext();
                Intent intent = new Intent(context, Main3Activity.class);
                intent.putExtra("id", id);
                context.startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return estudiantesArrayList.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder{

        TextView nombres, apellidos, parcial1, parcial2;
        ImageView foto;
        View view1;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            view1 = itemView;
            nombres = (TextView)itemView.findViewById(R.id.LBLNombresE);
            apellidos = (TextView)itemView.findViewById(R.id.LBLApellidosE);
            parcial1 = (TextView)itemView.findViewById(R.id.LBLParcial1E);
            parcial2 = (TextView)itemView.findViewById(R.id.LBLPArcial2E);
            foto = (ImageView)itemView.findViewById(R.id.ImagenEstudiante);
        }
    }
}

